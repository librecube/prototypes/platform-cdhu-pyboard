from pyb import LED, UART
import os
import json

import spacecan
import spacecan.services


# hardware setup
led_heartbeat = LED(1)


# define callback functions


def packet_monitor(service, subtype, data, node_id):
    if data:
        print(f"TM[{service:02}, {subtype:02}] with data {data} from node {node_id}")
    else:
        print(f"TM[{service:02}, {subtype:02}] with no data from node {node_id}")


def received_parameter_value_report(node_id, report):
    print(f"received parameter value report from node {node_id}:")
    for parameter_id, value in sorted(report.items()):
        parameter = pus.parameter_management.get_parameter(parameter_id)
        print(f" => {parameter.parameter_name}: {value}")


def received_housekeeping_report(node_id, report_id, report):
    print(f"received housekeeping report {report_id} from node {node_id}:")
    for parameter_id, value in sorted(report.items()):
        parameter = pus.parameter_management.get_parameter(parameter_id)
        print(f" => {parameter.parameter_name}: {value}")


# create node and configure packet utilization protocol


controller = spacecan.Controller.from_file("config/spacecan.json")
pus = spacecan.services.PacketUtilizationServiceController(controller)
pus.packet_monitor = packet_monitor

context = {}


# detect all responder nodes
for i in os.ilistdir("config"):
    if i[1] != 0x4000:
        continue  # not a directory
    files = os.listdir("config/" + i[0])

    if "spacecan.json" in files:
        with open(f"config/{i[0]}/spacecan.json", "r", encoding="utf-8") as f:
            config = json.load(f)
        if node_id := config.get("node_id"):
            # context["node_ids"].append(node_id)
            pus.parameter_management.add_parameters_from_file(
                f"config/{i[0]}/parameters.json", node_id=node_id
            )
            pus.housekeeping.add_housekeeping_reports_from_file(
                f"config/{i[0]}/housekeeping.json", node_id=node_id
            )
            pus.function_management.add_functions_from_file(
                f"config/{i[0]}/functions.json", node_id=node_id
            )


pus.test.received_connection_test_report = lambda node_id: print(
    f"received connection test report from {node_id}",
)

pus.parameter_management.received_parameter_value_report = (
    received_parameter_value_report
)
pus.housekeeping.received_housekeeping_report = received_housekeeping_report

controller.sent_heartbeat = lambda: led_heartbeat.toggle()
controller.connect()
controller.start()

INTRO_TEXT = """
    <Stop program with Ctrl-C>

    Enter commands using this pattern:   
        <n>,<s>,<t>,<data>
    
    where:
        n = node id
        s = service
        t = subtype
        data are comma seperated byte values, optional

        Example: 2,17,1 sends a test command (17,1) to node 2

    Enter 'h' to show this text again.
"""

print(INTRO_TEXT)

uart_A = UART(6, baudrate=115200, timeout=1000)
uart_B = UART(2, baudrate=115200, timeout=1000)

try:
    while True:
        x = uart_A.readline()
        if x is None:
            continue
        x = x.decode()
        x = x.strip()
        print("Received:", x)

        #     while True:
        #         x = input("").lower()

        if x == "h":
            print(INTRO_TEXT)

        elif x == "b":
            print("switch bus")
            controller.switch_bus()

        else:
            try:
                x = x.split(",")
                node_id = int(x[0])
                service = int(x[1])
                subtype = int(x[2])

            except Exception as e:
                print("malformed input:", e)
                continue

            case = (service, subtype)

            # enable periodic housekeeping reports
            if case == (3, 5):
                report_ids = [int(i) for i in x[3:]]
                pus.housekeeping.send_enable_period_housekeeping_reports(
                    node_id, report_ids
                )

            # disable periodic housekeeping reports
            elif case == (3, 6):
                report_ids = [int(i) for i in x[3:]]
                pus.housekeeping.send_disable_period_housekeeping_reports(
                    node_id, report_ids
                )

            # request single shot housekeeping reports
            elif case == (3, 27):
                report_ids = [int(i) for i in x[3:]]
                pus.housekeeping.send_single_shot_housekeeping_reports(
                    node_id, report_ids
                )

            # perform a function
            elif case == (8, 1):
                function_id = int(x[3])
                arguments = [int(y) for y in x[4:]] if len(x) > 4 else None
                pus.function_management.send_perform_function(
                    node_id, function_id, arguments
                )

            # connection test
            elif case == (17, 1):
                pus.test.send_connection_test(node_id)

            # application connection test
            elif case == (17, 3):
                apid = int(x[3])
                pus.test.send_application_connection_test(node_id, apid)

            # report parameter values
            elif case == (20, 1):
                parameter_ids = [int(i) for i in x[3:]]
                pus.parameter_management.send_report_parameter_values(
                    node_id, parameter_ids
                )

except KeyboardInterrupt:
    print()

controller.stop()
controller.disconnect()
